import React, { Component } from "react";
import "../App.css";

class HargaBuah extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataHargaBuah : [
         { nama: "Semangka", harga: 10000, berat: 1000 },
         { nama: "Anggur", harga: 40000, berat: 500 },
         { nama: "Strawberry", harga: 30000, berat: 400 },
         { nama: "Jeruk", harga: 30000, berat: 1000 },
         { nama: "Mangga", harga: 30000, berat: 500 },
      ],
      inputNama: "",
      inputHarga: "",
      inputBerat: "",
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
  
  handleChange(event) {
    var inama = event.target.inama
    var iharga = event.target.iharga
    var iberat = event.target.iberat
    this.setState({
      inputNama: inama,
      inputHarga: iharga,
      inputBerat:iberat
    })
    this.setState({ inputNama: event.target.inama });
  }

  handleSubmit(event) {
    event.preventDefault()
    console.log(this.state.inputNama)
    this.setState({
      dataHargaBuah: [...this.state.dataHargaBuah, [this.state.inputNama,this.state.inputHarga,this.state.inputBerat]],
      inputNama: "",
      inputBerat: "",
      inputHarga: ""
    });
  }
  handleDelete(event) {
    let index = event.target.inama;
    this.state.dataHargaBuah.splice(index, 1)
    console.log(this.state.dataHargaBuah)
    //this.setState({dataHargaBuah:this.state.dataHargaBuah})  
  }

  render() {
    return (
      <>
        <div>
          <h2 id="h2">Tabel Harga Buah</h2>
          <table>
            <div>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Harga</th>
                  <th>Berat</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataHargaBuah.map((val, index) => {
                  return (
                    <>
                      <tr>
                        <td>
                          {index+1}
                        </td>
                        <td>
                          {val.nama}
                        </td>
                        <td>
                          {val.harga}
                        </td>
                        <td>
                          {val.berat/1000 +"kg"}
                        </td>
                        <td>
                          <button>Edit</button>
                          <button value={index} onClick={this.handleDelete}>Delete</button>
                        </td>
                      </tr>
                    </>
                  );
                })}
              </tbody>              
            </div>
          </table>
        </div>
        <div>
          <h1>Form Buah</h1>
          <form onSubmit={this.handleSubmit} align = "center">
            <label>
              Nama :
            </label>
            <input type="text" value={this.state.inputNama} onChange={this.handleChange} />
            <br/>
            <label>
              Harga :
            </label>
            <input type="number" value={this.state.inputHarga} onChange={this.handleChange} />
            <br/>
            <label>
              Berat :
            </label>
            <input type="number" value={this.state.inputBerat} onChange={this.handleChange} />
             <br/>
            <button >Submit</button>
          </form>
        </div>
      </>
    )
  }
  
}


export default HargaBuah
