import React, { Component } from 'react';

class Timer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            time: 100,
            showTime: true,
            date: new Date(),
        }
    }
    componentDidMount() {
        if (this.props.start !== undefined) {
            this.setState({ time: this.props.start })
        }
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    componentDidUpdate() {
        if (this.state.time === 0) {
            this.state.showTime=false;
        }
           
    }    
    tick() {
        this.setState({
            time: this.state.time - 1,
            date: new Date()
        });
    }
    render() {
        return (
            <>
                {
                    this.state.showTime && (
                        <h3 style={{ textAlign: "center" }}>
                        Sekarang jam : {this.state.date.toLocaleTimeString()} &ensp;
                        Hitung mundur : {this.state.time}</h3>
                    )
                }
            </>
        )
    }
}
    export default Timer;