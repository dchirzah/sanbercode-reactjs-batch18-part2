import React, {useState, useEffect} from "react"
import axios from "axios"

const HooksWithAxios = ()=>{ 
  const [daftarBuah, setdaftarBuah] =  useState(null)
  const [input, setInput] =  useState({name: "",harga: "",berat:0, id: null})

  useEffect( () => {
    if (daftarBuah === null){
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then(res => {
        let daftarBuah = res.data
        setdaftarBuah(
          daftarBuah.map(el=> {
            return {id: el.id, name: el.name,harga:el.harga,berat:el.berat}
          })
        )      
      })
    }
  },[daftarBuah])

  const submitForm = (event) =>{
    event.preventDefault()
    
    if ( input.id === null){
      axios.post(`http://backendexample.sanbercloud.com/api/fruits`, { name: input.name})
      .then(res => {
        var data = res.data
        setdaftarBuah([...daftarBuah, {id: data.id, name: data.name}])
        setInput({id: null, name: ""})
      })

    }else{

      axios.put(`http://backendexample.sanbercloud.com/api/fruits/{ID_FRUIT}`, { name: input.name})
      .then(res => {
        var daftarBuah = daftarBuah.map(x => {
          if (x.id === input.id){
            x.nama = input.nama
          }
          return x
        })
        setdaftarBuah(daftarBuah)
        setInput({ id: null, name: "" })
        setInput({ harga: null, harga: "" })
        setInput({berat: null, berat: ""})
      })
    }
  }

  const handleDelete = (event) =>{
    var ID_FRUIT= parseInt(event.target.value) 
    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/{ID_FRUIT}`)
    .then(res => {
      var daftarBuah = daftarBuah.filter(x=> x.id !== ID_FRUIT)
      setdaftarBuah(daftarBuah)
    })
  }


  const changeInputName = (event) =>{
    var value= event.target.value
    setInput({...input, name: value})
  }

  const handleEdit = (event) =>{
    var ID_FRUIT= parseInt(event.target.value)
    var buah = daftarBuah.find(x=> x.id === ID_FRUIT)

    setInput({id: ID_FRUIT, name: buah.name})

  }

  return (
    <>
      <div style={{margin: "0 auto", width: "50%"}}>
        <h1 style={{textAlign: 'center'}}>Daftar Buah</h1>
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            { daftarBuah !== null && (
                daftarBuah.map((item, index)=>{
                  return(                    
                    <tr key={item.id}>
                      <td>{index+1}</td>
                      <td>{item.name}</td>
                      <td>{item.harga}</td>
                      <td>{item.berat}</td>
                      <td>
                        <button value={item.id} onClick={handleEdit}>Edit</button>
                        <button style={{marginLeft: "1em"}} value={item.id} onClick={handleDelete}>Delete</button>
                      </td>
                    </tr>
                  )
                })
            )}
          </tbody>
        </table>
        <br/>
        <br />
        
        <form style={{textAlign: "center"}} onSubmit={submitForm}>
            <strong style={{marginRight: "10px"}}>Nama</strong>
            <input required type="text" value={input.name} onChange={changeInputName}/>
            <button>Save</button>
        </form>
      </div>
    </>
  )
} 

export default HooksWithAxios
