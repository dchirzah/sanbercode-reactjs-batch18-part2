import React, {useState, useEffect} from "react"

const HooksFruit = ()=>{
  const [text, setText] = useState("")
  
  useEffect(() => {
    // Memperbarui judul dokumen menggunakan API browser
    document.title = `${text}`;
  });


  const onInputChange=(e)=>{
    let value = e.target.value;
    setText(value)
  }
  
  return (
  <div style={{textAlign: "center"}}>
    <h1>{text}</h1>
    <br/>
    <input type="text" placeholder="input text" onChange={onInputChange} />
  </div>
  )
} 

export default HooksFruit
