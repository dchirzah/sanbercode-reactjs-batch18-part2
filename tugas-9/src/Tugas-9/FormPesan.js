import React from "react";
import "../App.css";

function FormPesan() {
  return (
    <body>
      <form class="form">
        <div>
          <h1>Form Pembelian Buah</h1>
        </div>
        <div id="formRow">
          <div id="formLabel">
            <label for="nama">Nama Pelanggan</label>
          </div>
          <div id="formInput">
            <input type="text" />
            <br />
          </div>
        </div>
        <div id="formRow">
          <div id="formLabelCheck">
            <label for="buah">Daftar Item</label>
          </div>
          <div>
            <input type="checkbox" id="semangka" name="buah" value="Semangka" />
            <label for="semangka"> Semangka</label>
            <br />
            <input type="checkbox" id="jeruk" name="buah" value="Jeruk" />
            <label for="jeruk"> Jeruk</label>
            <br />
            <input type="checkbox" id="nanas" name="buah" value="Nanas" />
            <label for="nanas"> Nanas</label>
            <br />
            <input type="checkbox" id="salak" name="buah" value="Salak" />
            <label for="salak"> Salak</label>
            <br />
            <input type="checkbox" id="anggur" name="buah" value="Anggur" />
            <label for="anggur"> Anggur</label>
          </div>
        </div>
        <div>
          <br />
          <br />
          <input type="submit" value="Kirim" id="roundButton" />
        </div>
      </form>
    </body>
  );
}

export default FormPesan;
