import React, { Component } from "react";
import "../App.css";

class HargaBuah extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataHargaBuah : [
         { nama: "Semangka", harga: 10000, berat: 1000 },
         { nama: "Anggur", harga: 40000, berat: 500 },
         { nama: "Strawberry", harga: 30000, berat: 400 },
         { nama: "Jeruk", harga: 30000, berat: 1000 },
         { nama: "Mangga", harga: 30000, berat: 500 },
      ],
      inputNama: "",
      inputHarga: "",
      inputBerat: 0,
      //array tidakk punya index -1
      indexOfForm:-1
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
  handleDelete(event) {
    let index = event.target.value;
    let newDaftarBuah = this.state.dataHargaBuah
    let editedDataBuah = newDaftarBuah[this.state.indexOfForm]
    newDaftarBuah.splice(index,1)
    
    if (editedDataBuah !== undefined) {
      //array findIndex baru ada di ES6
      var newIndex = newDaftarBuah.findIndex((el) => el === editedDataBuah)
      this.setState({dataHargaBuah: newDaftarBuah, indexOfForm:newIndex})
    } else {
      this.setState({dataHargaBuah: newDaftarBuah})
    }
  }
  
  handleEdit(event) {
    let index = event.target.value
    let dataHargaBuah = this.state.newDaftarBuah[index]
    this.setState({
      inputNama: dataHargaBuah.nama,
      inputHarga: dataHargaBuah.harga,
      inputBerat: dataHargaBuah.berat,
      indexOfForm: index
    })
  }

  handleChange(event) {
    let typeOfInput = event.target.nama
    switch (typeOfInput) {
      case "nama":
        {
          this.setState({ inputNama: event.target.value });
          break
        }
      case "harga":
        {
          this.setState({ inputHarga: event.target.value });
          break
        }
      case "berat":
        {
          this.setState({ inputBerat: event.target.value });
          break
        }
      default:
        { break;}
    }
  }

  handleSubmit(event) {
    //menahan submit
    event.preventDefault()

    let nama= this.state.inputNama
    let harga= this.state.inputHarga.toString()
    let berat= this.state.inputBerat

    let newDaftarBuah = this.state.dataHargaBuah
    let index = this.state.indexOfForm

    if (index === -1) {
      newDaftarBuah[index] = {nama, harga, berat}
    }
    this.setState({
      dataHargaBuah: newDaftarBuah,
      inputNama: "",
      inputHarga: "",
      inputBerat: 0
    })
  }
  

  render() {
    return (
      <>
        <div>
          <h2 id="h2">Tabel Harga Buah</h2>
          <table id="buah">
            <div>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Harga</th>
                  <th>Berat</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataHargaBuah.map((val, index) => {
                  return (
                    <>
                      <tr key={index}>
                        <td>
                          {index + 1}
                        </td>
                        <td>
                          {val.nama}
                        </td>
                        <td>
                          {val.harga}
                        </td>
                        <td>
                          {val.berat / 1000 + "kg"}
                        </td>
                        <td>
                          <button>Edit</button>
                          <button value={index} onClick={this.handleDelete}>Delete</button>
                        </td>
                      </tr>
                    </>
                  );
                })}
              </tbody>
            </div>
          </table>
        </div>
        <div>
          <h1>Form Buah</h1>
          <form onSubmit={this.handleSubmit} align="center">
            <label>
              Nama :
            </label>
            <input type="text" value={this.state.inputNama} onChange={this.handleChange} />
            <br />
            <label>
              Harga :
            </label>
            <input type="number" value={this.state.inputHarga} onChange={this.handleChange} />
            <br />
            <label>
              Berat :
            </label>
            <input type="number" value={this.state.inputBerat} onChange={this.handleChange} />
            <br />
            <button >Submit</button>
          </form>
        </div>
      </>
    )
  }
}


export default HargaBuah
