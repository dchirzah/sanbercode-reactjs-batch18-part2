import React, { Component } from "react";

class TampilNama extends Component {
  render() {
    return (
      <>
        {this.props.namabuah}
        {this.props.harga}
        {this.props.berat}
      </>
    );
  }
}
class DaftarBuah extends Component {
  render() {
    return (
      <>
        <div>
          <h2 id="h2">Tabel Harga Buah</h2>
          <table>
            <div>
              <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
              </tr>
              {dataHargaBuah.map((item) => {
                return (
                  <>
                    <tr>
                      <td>
                        <TampilNama namabuah={item.nama} />
                      </td>
                      <td>
                        <TampilNama harga={item.harga} />
                      </td>
                      <td>
                        <TampilNama berat={item.berat/1000+" kg"} />
                      </td>
                    </tr>
                  </>
                );
              })}
            </div>
          </table>
        </div>
      </>
    );
  }
}
let dataHargaBuah = [
  { nama: "Semangka", harga: 10000, berat: 1000 },
  { nama: "Anggur", harga: 40000, berat: 500 },
  { nama: "Strawberry", harga: 30000, berat: 400 },
  { nama: "Jeruk", harga: 30000, berat: 1000 },
  { nama: "Mangga", harga: 30000, berat: 500 },
];

export default DaftarBuah;
